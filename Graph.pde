/**
 * Visualize Exponential Decay
 */
import java.util.LinkedList;
import java.util.ListIterator;

class Graph {

  private final float offset;   // 
  
  private final float xIncr = 1/frameRate;    // x increment. Move right by how much? 1/ frameRate would be one px per second

  public Graph() {
    offset = width - canvasWidth + 10;
  }
  
  private float timeToX(float time) {
    // time may not be the best choice of a var name if we are going to calculate an actual time
    return time * xIncr;
  }
  
  private float popToY(float pop) {
    float percentLeft = pop / initialPopulation;
    float cartesianY = percentLeft * height;
    float pixelY = height - cartesianY;
    return pixelY;
  }

  /**
   * 
   */
  private void show(float time, float population) {
    stroke(239, 82, 92);  
    
    // TO DO
      // do time in seconds according to frame rate
      // calculate decay constant in Bq (decay events per second)
      // do lots of other math to make this all worth while
      // refactor colors and window sizes to a MySettings or something
    
    float x = timeToX(time);
    float y = popToY(population); 
    
    // save our default state
    pushMatrix();
    // move over past the canvas, and down to the bottom
    translate(offset, 0);
    // rotate around like a cartesian plane where higher y is up (as opposed to pixel coordinates)
    // rotateZ(radians(180)); // unfortunately screws up x although this makes y easier
    stroke(239, 82, 92);
    strokeWeight(2);
    point(x, y);
    // undo these changes to our state
    popMatrix();
    
  }

}

