/**
 * The hole a ball can fall into, thus decaying the set of balls
 */

class Hole {

  /** proportion of hole length and width to canvas length and width **/
  private final float PROPORTION = .2;
  
  float x;
  float y;
  float h;
  float w;

  public Hole() {
    
    // Feel free to change these. It shouldn't break anything.
    this.x = canvasWidth / 2;
    this.y = canvasHeight / 2;
    this.h = canvasHeight * PROPORTION;
    this.w = canvasWidth * PROPORTION;
  }

  public void show() {
    rectMode(CENTER);
    stroke(0);
    fill(40);
    rect(x, y, w, h);
  }

  /**
   * return whether or not something at x,y is trapped in the hole
   */
  public boolean trapped(Ball b) {
    float bx = b.location.x;
    float by = b.location.y;
    if (   ( bx > (this.x - w/2) && bx < (this.x + w/2) ) 
       &&  ( by > (this.y - h/2) && by < (this.y + h/2) )  ) {
      return true;
    } 
    else {
      return false;
    }
  }
}

