/**
 * Note: In the Processing Environment, Separate Tabs are interpretted as inner classes.
 *
 */

class Ball {
  private PVector location;
  private PVector movement;
  private float speedup;
  private float diameter;
  private boolean dead;


  public Ball (PVector location, PVector movement, float diameter, float speedup) {
    this.location = location;
    movement.normalize();
    movement.mult(SPEEDUP);
    this.movement = movement;
    this.dead = false;
    this.diameter = diameter;
    this.diameter = speedup;
  }

  public void bounceX() {
    movement.x = -1 * movement.x;
  }

  public void bounceY() {
    movement.y = -1 * movement.y;
  }

  public void move() {
    
    // adding notion of direction in here would be another edge case OK solutions
    
    if (location.x < (DIAMETER * SPEEDUP) || location.x > (canvasWidth - SPEEDUP * DIAMETER)) {
      bounceX();
    }
    if (location.y < 0 || location.y > canvasHeight) {
      bounceY();
    }
    location.add(movement);
  }

  public void show() {
    fill(239, 82, 92);
    stroke(229, 72, 82);
    ellipse(location.x, location.y, DIAMETER, DIAMETER);
  }
  
  public void die() {
    dead = true;
  }
  
  public boolean isAlive() {
    return !dead;
  }
}

