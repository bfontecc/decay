/**
 * A stochastic model of Exponential Decay, represented as balls randomly falling into a hole.
 *
 * Euclidean Vectors in Processing's PVector class are used to represent the ball's motion.
 */

final float initialPopulation = 200;
float population;
Ball[] a;
Hole h;
Graph graph;
boolean running;
float canvasWidth;
float canvasHeight;
float time;
final float DIAMETER = 1;
final float SPEEDUP = 4;

void setup() {
  size(800, 400, P3D);
  background(color(90, 180, 160));
  canvasHeight = height;
  canvasWidth = width - 400;
  a = new Ball[(int) initialPopulation];
  h = new Hole();
  graph = new Graph();
  for (int i = 0; i < initialPopulation; i++) {
    PVector l = new PVector(random(canvasWidth - DIAMETER * SPEEDUP), random(canvasHeight - DIAMETER * SPEEDUP));
    PVector m = PVector.random2D();
    a[i] = new Ball(l, m, DIAMETER, SPEEDUP);
  }
  population = initialPopulation;
  time = 0;
  running = true;
}

void draw() {
  if (running) {
    rectMode(CORNER);
    fill(color(90, 180, 160));
    noStroke();
    rect(0, 0, canvasWidth, canvasHeight);
    frameRate(30);
    stroke(0);
    line(canvasWidth, canvasHeight, canvasWidth, 0);
    h.show();
    time++;
    for (int i = 0; i < initialPopulation; i++) {
      Ball b = a[i];
      if (b.isAlive()) {
        b.move();
        if (h.trapped(b)) {
          b.die();
          population--;
        } 
        else {
          b.show();
        }
      }
    }
  }
  graph.show(time, population);
}

void keyPressed() {
  running = false;
}

